import React, { useState, useEffect } from 'react'
import './modal-friend.scss'
import { connect } from 'react-redux';
import { closeModal, addFriend, editFriend } from 'redux/action'
import { Modal, Button, Form, FormGroup, ControlLabel, HelpBlock, Input, Notification } from 'rsuite'
import { UserPlusIcon, EditIcon } from 'components/icons/icons'

type dataFriend = Array<{
  id: number,
  name: string,
  address: string,
  phoneNumber: string,
}>

type Props = {
  typeModal: string,
  isActive: boolean,
  friendID: number,
  dispatch: any,
  dataFriends: dataFriend,
}

const ModalFriend = ({typeModal, isActive, friendID, dispatch, dataFriends} : Props) => {  
  const [dataName, setDataName] = useState('')
  const [dataAddress, setDataAddress] = useState('')
  const [dataPhoneNumber, setDataPhoneNumber] = useState('')

  const close = () => {
    dispatch(closeModal())
  }

  useEffect(() => {
    if (typeModal === 'edit') {
      setDataName(dataFriends[friendID - 1].name)
      setDataAddress(dataFriends[friendID - 1].address)
      setDataPhoneNumber(dataFriends[friendID - 1].phoneNumber)
    }
    console.log('run');
    
  }, [dataFriends, friendID, isActive, typeModal])

  const submit = () => {
    const validatePhoneNumber = () => {
      if ((/^\D?(\d{3})\D?\D?(\d{3})\D?(\d{4})$/g).test(dataPhoneNumber)) {
        return true
      }
      else {
        openNotification('Invalid Phone Number ! Somethings was wrong with you phone number !')
        return false
      }
    }

    const validateName = () => {
      if (dataName === '') {
        openNotification('Invalid Name ! You must input field name !')
        return false
      }
      else {
        return true
      }
    }

    const validateAddress = () => {
      if (dataAddress === '') {
        openNotification('Invalid Address ! You must input field address !')
        return false
      }
      else {
        return true
      }
    }
    if (typeModal === 'add' && validateName() && validateAddress() && validatePhoneNumber()) {
      close()
      dispatch(addFriend(dataName, dataAddress, dataPhoneNumber))
      setDataName('')
      setDataAddress('')
      setDataPhoneNumber('')
    }
    if (typeModal === 'edit' && validateName() && validateAddress() && validatePhoneNumber()) {
      close()
      dispatch(editFriend(friendID, dataName, dataAddress, dataPhoneNumber))
      setDataName('')
      setDataAddress('')
      setDataPhoneNumber('')
    }
  }

  const openNotification = (des: any) => {
    Notification['error']({
      title: 'Error',
      description: des
    });
  }

  return (
    <div className="modal-friend">
      <Modal backdrop={true} show={isActive} onHide={close}>
        <Modal.Header>
          {typeModal === 'add' && <Modal.Title><UserPlusIcon />Add Friend</Modal.Title>}
          {typeModal === 'edit' && <Modal.Title><EditIcon />Edit Friend</Modal.Title>}
        </Modal.Header>
        <Modal.Body>
        <Form onKeyPress={(e: any) => { if (e.key === 'Enter') submit()} } >
          <FormGroup>
            <ControlLabel>Name</ControlLabel>
            <Input onChange={(value) => {setDataName(value)}} value={dataName} name="name" type="text" />
            <HelpBlock tooltip>Required</HelpBlock>
          </FormGroup>
          <FormGroup>
            <ControlLabel>Address</ControlLabel>
            <Input onChange={(value) => {setDataAddress(value)}} value={dataAddress} name="name" type="text" />
            <HelpBlock tooltip>Required</HelpBlock>
          </FormGroup>
          <FormGroup>
            <ControlLabel>Phone Number</ControlLabel>
            <Input onChange={(value) => {setDataPhoneNumber(value)}} value={dataPhoneNumber} name="name" type="text" />
            <HelpBlock tooltip>Required</HelpBlock>
          </FormGroup>
        </Form>
        </Modal.Body>
        <Modal.Footer>
          {typeModal === 'add' && <Button onClick={submit} appearance="primary">Add Friend</Button>}
          {typeModal === 'edit' && <Button onClick={submit} appearance="primary">Save</Button>}
          <Button onClick={close} appearance="subtle">
            Cancel
          </Button>
        </Modal.Footer>
      </Modal>
    </div>
  )
}

const mapStateToProps = (state: any) => ({
  typeModal: state.ModalFriend.typeModal,
  isActive: state.ModalFriend.isActive,
  friendID: state.ModalFriend.friendID,
  dataFriends: state.DataFriends,
})

export default connect(mapStateToProps)(ModalFriend);