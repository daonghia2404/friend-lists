import React from 'react'
import './friend-lists.scss'
import FriendCard from 'components/friend-card/fiend-card'
import { connect } from 'react-redux';
import { FaceSadIcon } from 'components/icons/icons'

type dataFriends = Array<{
  id: number,
  name: string,
  address: string,
  phoneNumber: string,
}>

type Props = {
  dataFriends: dataFriends,
}

const FiendLists = ({ dataFriends }: Props) => {
  return (
    <div className="list-friends d-flex flex-wrap ">
      {dataFriends.length === 0 && 
        <div className="list-null d-flex flex-md-column justify-content-center align-items-center">
          <FaceSadIcon />
          <p>Wahh.. Look like you don't have friend.. Let's add some friend now !</p>
        </div>
      }
      {dataFriends.length > 0 && dataFriends.map(item => <FriendCard key={item.id} id={item.id} name={item.name} address={item.address} phoneNumber={item.phoneNumber} />)}
    </div>
  )
}

const mapStateToProps = (state: any) => ({
  dataFriends: state.DataFriends,
})

export default connect(mapStateToProps)(FiendLists);